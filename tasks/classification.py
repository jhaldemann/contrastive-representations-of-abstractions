import torch
from tasks.task import Task
from data.settings import device, deftype
import benchmark.loss as L

class ClassificationRegressive(Task):
    def __init__(self, param, featuremap, dataset, testset):
        super().__init__("classification regressive", param, featuremap, dataset, testset)
        self.class_weights = self.dataset.class_weights
        self.loss = L.MultiLabelLoss(self.param, self.featuremap, self.class_weights)
        self.classes = ["polynomial","periodic","exponential","trigonometric","modulo","prime","finite"]
        self.epoch_valid_metrics = {
            "polynomial": {"TP":0, "TN":0, "FP":0, "FN":0},
            "periodic": {"TP":0, "TN":0, "FP":0, "FN":0},
            "exponential": {"TP":0, "TN":0, "FP":0, "FN":0},
            "trigonometric": {"TP":0, "TN":0, "FP":0, "FN":0},
            "modulo": {"TP":0, "TN":0, "FP":0, "FN":0},
            "prime": {"TP":0, "TN":0, "FP":0, "FN":0},
            "finite": {"TP":0, "TN":0, "FP":0, "FN":0},
        }

    def get_model_head(self):
        from models.classification import ClassificationRegressive
        return ClassificationRegressive(self.param)

    def prepare_batch(self, x, y):
        classes = y[:,0:7]
        return x, classes

    def predict(self, x, y, model, task_name=None):
        if task_name:
            y_pred = model(x)[task_name]
        else:
            y_pred = model(x)
        y_true = y
        return y_pred, y_true

    def loss_train(self, y_pred, y_true):
        return self.loss(y_pred, y_true)

    def loss_valid(self, y_pred, y_true):
        return self.loss(y_pred, y_true)

    def validate(self, x, y, y_pred, y_true, model, im=None, task_name=None):
        y_pred_sig = torch.sigmoid(y_pred)
        del x,y,y_pred
        confidence_threshold = self.param["classification_confidence_threshold"]
        y_pred_classes = torch.zeros(y_pred_sig.shape, device=device)
        y_pred_classes[y_pred_sig >= confidence_threshold] = 1
        del y_pred_sig

        for i, c in enumerate(self.classes):
            true_positive = torch.count_nonzero((y_pred_classes[:,i] == 1) & (y_true[:,i] == 1)).item()
            true_negative = torch.count_nonzero((y_pred_classes[:,i] == 0) & (y_true[:,i] == 0)).item()
            false_positive = torch.count_nonzero((y_pred_classes[:,i] == 1) & (y_true[:,i] == 0)).item()
            false_negative = torch.count_nonzero((y_pred_classes[:,i] == 0) & (y_true[:,i] == 1)).item()
            self.epoch_valid_metrics[c]["TP"] += true_positive
            self.epoch_valid_metrics[c]["TN"] += true_negative
            self.epoch_valid_metrics[c]["FP"] += false_positive
            self.epoch_valid_metrics[c]["FN"] += false_negative

            self.batch_valid_metrics["classification/TP/"+str(c)] = true_positive
            self.batch_valid_metrics["classification/TN/"+str(c)] = true_negative
            self.batch_valid_metrics["classification/FP/"+str(c)] = false_positive
            self.batch_valid_metrics["classification/FN/"+str(c)] = false_negative

    def evaluate_valid_epoch(self, epoch, batch_count):
        result = {}
        global_tp = 0
        global_fp = 0
        global_fn = 0
        global_precision = 0
        global_recall = 0
        for c in self.classes:
            tp = self.epoch_valid_metrics[c]["TP"]
            tn = self.epoch_valid_metrics[c]["TN"]
            fp = self.epoch_valid_metrics[c]["FP"]
            fn = self.epoch_valid_metrics[c]["FN"]
            class_accuracy = (tp+tn) / (tp+tn+fp+fn) if (tp+tn+fp+fn)!=0 else 0
            class_precision = tp / (fp+tp) if (fp+tp) != 0 else 1
            class_recall = tp / (fn+tp) if (fn+tp) != 0 else 1
            class_f1 = (2*class_precision*class_recall) / (class_precision+class_recall) if (class_precision+class_recall) != 0 else 0
            global_tp += tp
            global_fp += fp
            global_fn += fn
            global_precision += class_precision
            global_recall += class_recall
            
            result["classification/accuracy/"+str(c)] = class_accuracy
            result["classification/precision/"+str(c)] = class_precision
            result["classification/recall/"+str(c)] = class_recall
            result["classification/f1_score/"+str(c)] = class_f1

        global_precision /= len(self.classes)
        global_recall /= len(self.classes)

        macro_average_f1 = (2*global_precision*global_recall) / (global_precision+global_recall) if (global_precision+global_recall) != 0 else 0
        micro_precision = global_tp / (global_fp + global_tp) if (global_fp+global_tp) != 0 else 0
        micro_recall = global_tp / (global_fn + global_tp) if (global_fn+global_tp) != 0 else 0
        micro_average_f1 = (2*micro_precision*micro_recall) / (micro_precision+micro_recall) if (micro_precision+micro_recall) != 0 else 0
        result["classification/f1_score/macro_average"] = macro_average_f1
        result["classification/f1_score/micro_average"] = micro_average_f1
        for key in self.epoch_valid_metrics:
            self.epoch_valid_metrics[key] = {"TP":0, "TN":0, "FP":0, "FN":0}
        return result

from pytorch_metric_learning.losses import NTXentLoss, SupConLoss, ContrastiveLoss, TripletMarginLoss

class ClassificationContrastive(Task):
    def __init__(self, param, featuremap, dataset, testset):
        super().__init__("classification contrastive", param, featuremap, dataset, testset)
        self.epoch_train_metrics = {
            "classification/epoch_train/contrastive loss": 0,
        }
        self.epoch_valid_metrics = {
            "classification/epoch_top_k_accuracy/top1": 0,
            "classification/epoch_top_k_accuracy/top3": 0,
            "classification/epoch_top_k_accuracy/top5": 0,
            "classification/epoch_valid/contrastive loss": 0,
        }

        if param["classification_contrastive_loss"] == "ntxent":
            self.loss_func = NTXentLoss(temperature=param["classification_temperature"])
        elif param["classification_contrastive_loss"] == "supcon":
            self.loss_func = SupConLoss(temperature=param["classification_temperature"])
        elif param["classification_contrastive_loss"] == "contrastive":
            self.loss_func = ContrastiveLoss(neg_margin=1)
        elif param["classification_contrastive_loss"] == "triplet":
            self.loss_func = TripletMarginLoss(margin=0.2)

    def get_model_head(self):
        from models.classification import ClassificationContrastive
        return ClassificationContrastive(self.param)

    def prepare_batch(self, x, y):
        return x, y

    def predict(self, x, y, model, task_name=None):
        labels = torch.argmax(y[:,0:7], axis=1)

        if task_name:
            x_embeddings = model(x)[task_name]
        else:
            x_embeddings = model(x)
        return x_embeddings, labels
        
    def loss_train(self, y_pred, y_true):
        loss = self.loss_func(y_pred, y_true)
        self.batch_train_metrics["classification/batch_train/contrastive loss"] = loss.mean().item()
        self.epoch_train_metrics["classification/epoch_train/contrastive loss"] += loss.mean().item()
        return loss

    def loss_valid(self, y_pred, y_true):
        loss = self.loss_func(y_pred, y_true)
        self.batch_valid_metrics["classification/batch_valid/contrastive loss"] = loss.mean().item()
        self.epoch_valid_metrics["classification/epoch_valid/contrastive loss"] += loss.mean().item()
        return loss

    def validate(self, x, y, y_pred, y_true, model, im, task_name=None):
        if task_name:
            distances, indices = im[task_name].get_nearest_neighbors(x, k=5)
        else:
            distances, indices = im.get_nearest_neighbors(x, k=5)
        
        topk_classes = self.dataset[indices.cpu()][1].to(device)
        topk_classes = topk_classes[:,:,0:7]

        batch_size = x.shape[0]
        left_classes = y[:,0:7].unsqueeze(1) # (L-batch, 1, classes)

        hits_all_classes = left_classes == topk_classes # (L-batch, 5, classes)
        all_classes = torch.all(hits_all_classes, dim=2) # (L-batch, 5)

        top1_hits = all_classes[:,0] # (batch,)
        top3_hits = torch.max(all_classes[:,0:3],dim=1)[0] # (batch,)
        top5_hits = torch.max(all_classes[:,0:5],dim=1)[0] # (batch,)

        top1_accuracy = top1_hits.sum() / batch_size
        top3_accuracy = top3_hits.sum() / batch_size
        top5_accuracy = top5_hits.sum() / batch_size

        self.batch_valid_metrics["classification/batch_top_k_accuracy/top1"] = top1_accuracy.item()
        self.batch_valid_metrics["classification/batch_top_k_accuracy/top3"] = top3_accuracy.item()
        self.batch_valid_metrics["classification/batch_top_k_accuracy/top5"] = top5_accuracy.item()
        self.epoch_valid_metrics["classification/epoch_top_k_accuracy/top1"] += top1_accuracy.item()
        self.epoch_valid_metrics["classification/epoch_top_k_accuracy/top3"] += top3_accuracy.item()
        self.epoch_valid_metrics["classification/epoch_top_k_accuracy/top5"] += top5_accuracy.item()