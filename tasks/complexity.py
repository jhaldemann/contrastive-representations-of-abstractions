import torch
from tasks.task import Task
from data.settings import device, deftype
import benchmark.loss as L

class ComplexityRegressive(Task):
    def __init__(self, param, featuremap, dataset, testset):
        super().__init__("complexity regressive", param, featuremap, dataset, testset)
        self.loss = L.MeanSquaredError(self.param, self.featuremap)
        self.epoch_train_metrics = {
            "complexity/epoch_train/mse-depth": 0,
            "complexity/epoch_train/mse-literals": 0,
            "complexity/epoch_train/mse-terminals": 0,
        }
        self.epoch_valid_metrics = {
            "complexity/epoch_valid/mse-depth": 0,
            "complexity/epoch_valid/mse-literals": 0,
            "complexity/epoch_valid/mse-terminals": 0,
        }

    def get_model_head(self):
        from models.complexity import ComplexityRegressive
        return ComplexityRegressive(self.param)

    def prepare_batch(self, x, y):
        complexity = y[:,7:10]
        return x, complexity

    def predict(self, x, y, model, task_name=None):
        if task_name:
            y_pred = model(x)[task_name]
        else:
            y_pred = model(x)
        y_true = y
        return y_pred, y_true

    def loss_train(self, y_pred, y_true):
        depth_loss = self.loss(y_pred[:,0], y_true[:,0])
        literals_loss = self.loss(y_pred[:,1], y_true[:,1])
        terminals_loss = self.loss(y_pred[:,2], y_true[:,2])
        self.batch_train_metrics["complexity/batch_train/mse-depth"] = depth_loss.item()
        self.batch_train_metrics["complexity/batch_train/mse-literals"] = literals_loss.item()
        self.batch_train_metrics["complexity/batch_train/mse-terminals"] = terminals_loss.item()
        self.epoch_train_metrics["complexity/epoch_train/mse-depth"] += depth_loss.item()
        self.epoch_train_metrics["complexity/epoch_train/mse-literals"] += literals_loss.item()
        self.epoch_train_metrics["complexity/epoch_train/mse-terminals"] += terminals_loss.item()
        return depth_loss + literals_loss + terminals_loss

    def loss_valid(self, y_pred, y_true):
        depth_loss = self.loss(y_pred[:,0], y_true[:,0])
        literals_loss = self.loss(y_pred[:,1], y_true[:,1])
        terminals_loss = self.loss(y_pred[:,2], y_true[:,2])
        self.batch_valid_metrics["complexity/batch_valid/mse-depth"] = depth_loss.item()
        self.batch_valid_metrics["complexity/batch_valid/mse-literals"] = literals_loss.item()
        self.batch_valid_metrics["complexity/batch_valid/mse-terminals"] = terminals_loss.item()
        self.epoch_valid_metrics["complexity/epoch_valid/mse-depth"] += depth_loss.item()
        self.epoch_valid_metrics["complexity/epoch_valid/mse-literals"] += literals_loss.item()
        self.epoch_valid_metrics["complexity/epoch_valid/mse-terminals"] += terminals_loss.item()
        return depth_loss + literals_loss + terminals_loss

    def validate(self, x, y, y_pred, y_true, model, im=None, task_name=None):
        pass

from pytorch_metric_learning.losses import NTXentLoss, SupConLoss, ContrastiveLoss, TripletMarginLoss

class ComplexityContrastive(Task):
    def __init__(self, param, featuremap, dataset, testset):
        super().__init__("similarity contrastive", param, featuremap, dataset, testset)
        self.epoch_train_metrics = {
            "complexity/epoch_train/contrastive loss": 0,
        }
        self.epoch_valid_metrics = {
            "complexity/epoch_top_k_depth_mse/top1": 0,
            "complexity/epoch_top_k_depth_mse/top3": 0,
            "complexity/epoch_top_k_depth_mse/top5": 0,
            "complexity/epoch_top_k_literal_mse/top1": 0,
            "complexity/epoch_top_k_literal_mse/top3": 0,
            "complexity/epoch_top_k_literal_mse/top5": 0,
            "complexity/epoch_top_k_terminal_mse/top1": 0,
            "complexity/epoch_top_k_terminal_mse/top3": 0,
            "complexity/epoch_top_k_terminal_mse/top5": 0,
            "complexity/epoch_valid/contrastive loss": 0,
        }

        if param["complexity_contrastive_loss"] == "ntxent":
            self.loss_func = NTXentLoss(temperature=param["complexity_temperature"])
        elif param["complexity_contrastive_loss"] == "supcon":
            self.loss_func = SupConLoss(temperature=param["complexity_temperature"])
        elif param["complexity_contrastive_loss"] == "contrastive":
            self.loss_func = ContrastiveLoss(neg_margin=1)
        elif param["complexity_contrastive_loss"] == "triplet":
            self.loss_func = TripletMarginLoss(margin=0.2)

    def get_model_head(self):
        from models.complexity import ComplexityContrastive
        return ComplexityContrastive(self.param)

    def prepare_batch(self, x, y):
        return torch.squeeze(x), torch.squeeze(y)

    def predict(self, x, y, model, task_name=None):
        complexity = y[:,7:10]

        pairs, labels = self.dataset.complexity_loss(x, complexity, self.param)

        if task_name:
            x_embeddings = model(pairs)[task_name]
        else:
            x_embeddings = model(pairs)

        return x_embeddings, labels
        
    def loss_train(self, y_pred, y_true):
        loss = self.loss_func(y_pred, y_true)
        self.batch_train_metrics["complexity/batch_train/contrastive loss"] = loss.mean().item()
        self.epoch_train_metrics["complexity/epoch_train/contrastive loss"] += loss.mean().item()
        return loss

    def loss_valid(self, y_pred, y_true):
        loss = self.loss_func(y_pred, y_true)
        self.batch_valid_metrics["complexity/batch_valid/contrastive loss"] = loss.mean().item()
        self.epoch_valid_metrics["complexity/epoch_valid/contrastive loss"] += loss.mean().item()
        return loss

    def validate(self, x, y, y_pred, y_true, model, im, task_name=None):
        complexity = y[:,7:10].unsqueeze(1)

        if task_name:
            distances, indices = im[task_name].get_nearest_neighbors(x, k=5)
        else:
            distances, indices = im.get_nearest_neighbors(x, k=5)
        
        topk_classes = self.dataset[indices.cpu()][1].to(device)

        topk_depth_square_error = torch.square(topk_classes[:,:,7] - complexity[:,:,0]) # (batch, 5)
        topk_literal_square_error = torch.square(topk_classes[:,:,8] - complexity[:,:,1]) # (batch, 5)
        topk_terminal_square_error = torch.square(topk_classes[:,:,9] - complexity[:,:,2]) # (batch, 5)

        top1_depth_mse = topk_depth_square_error[:,0].mean().item()
        top3_depth_mse = torch.min(topk_depth_square_error[:,0:3],dim=1)[0].mean().item()
        top5_depth_mse = torch.min(topk_depth_square_error[:,0:5],dim=1)[0].mean().item()

        top1_literal_mse = topk_literal_square_error[:,0].mean().item()
        top3_literal_mse = torch.min(topk_literal_square_error[:,0:3],dim=1)[0].mean().item()
        top5_literal_mse = torch.min(topk_literal_square_error[:,0:5],dim=1)[0].mean().item()

        top1_terminal_mse = topk_terminal_square_error[:,0].mean().item()
        top3_terminal_mse = torch.min(topk_terminal_square_error[:,0:3],dim=1)[0].mean().item()
        top5_terminal_mse = torch.min(topk_terminal_square_error[:,0:5],dim=1)[0].mean().item()

        self.batch_valid_metrics["complexity/batch_top_k_depth_mse/top1"] = top1_depth_mse
        self.batch_valid_metrics["complexity/batch_top_k_depth_mse/top3"] = top3_depth_mse
        self.batch_valid_metrics["complexity/batch_top_k_depth_mse/top5"] = top5_depth_mse
        self.epoch_valid_metrics["complexity/epoch_top_k_depth_mse/top1"] += top1_depth_mse
        self.epoch_valid_metrics["complexity/epoch_top_k_depth_mse/top3"] += top3_depth_mse
        self.epoch_valid_metrics["complexity/epoch_top_k_depth_mse/top5"] += top5_depth_mse

        self.batch_valid_metrics["complexity/batch_top_k_literal_mse/top1"] = top1_literal_mse
        self.batch_valid_metrics["complexity/batch_top_k_literal_mse/top3"] = top3_literal_mse
        self.batch_valid_metrics["complexity/batch_top_k_literal_mse/top5"] = top5_literal_mse
        self.epoch_valid_metrics["complexity/epoch_top_k_literal_mse/top1"] += top1_literal_mse
        self.epoch_valid_metrics["complexity/epoch_top_k_literal_mse/top3"] += top3_literal_mse
        self.epoch_valid_metrics["complexity/epoch_top_k_literal_mse/top5"] += top5_literal_mse

        self.batch_valid_metrics["complexity/batch_top_k_terminal_mse/top1"] = top1_terminal_mse
        self.batch_valid_metrics["complexity/batch_top_k_terminal_mse/top3"] = top3_terminal_mse
        self.batch_valid_metrics["complexity/batch_top_k_terminal_mse/top5"] = top5_terminal_mse
        self.epoch_valid_metrics["complexity/epoch_top_k_terminal_mse/top1"] += top1_terminal_mse
        self.epoch_valid_metrics["complexity/epoch_top_k_terminal_mse/top3"] += top3_terminal_mse
        self.epoch_valid_metrics["complexity/epoch_top_k_terminal_mse/top5"] += top5_terminal_mse