import torch
from tasks.task import Task
from data.settings import device, deftype
import benchmark.loss as L

class ContinuationRegressive(Task):
    def __init__(self, param, featuremap, dataset, testset):
        super().__init__("continuation regressive", param, featuremap, dataset, testset)
        self.epoch_train_metrics = {
            "continuation_reg/epoch/train_msle": 0
        }
        self.epoch_valid_metrics = {
            "continuation_reg/epoch/valid_msle": 0
        }
        self.loss = L.MeanSquaredError(self.param, self.featuremap)

    def get_model_head(self):
        from models.continuation import ContinuationRegressive
        return ContinuationRegressive(self.param)

    def prepare_batch(self, x, y):
        x_masked = x.clone()
        x_masked[:,-1] = 0
        x_masked[:,-1,self.featuremap.mask] = 1
        x[:,-1,self.featuremap.mask] = 1
        return x_masked, x[:,-1]

    def predict(self, x, y, model, task_name=None):
        if task_name:
            y_pred = model(x)[task_name]
        else:
            y_pred = model(x)
        y_true = y
        return y_pred, y_true

    def loss_train(self, y_pred, y_true):
        log = self.featuremap.log
        y_true_log = y_true[:,log]
        log_loss = self.loss(y_pred, y_true_log)
        self.batch_train_metrics["continuation_reg/batch/train_msle"] = log_loss.item()
        self.epoch_train_metrics["continuation_reg/epoch/train_msle"] += log_loss.item()
        return log_loss

    def loss_valid(self, y_pred, y_true):
        log = self.featuremap.log
        y_true_log = y_true[:,log]
        log_loss = self.loss(y_pred, y_true_log)
        self.batch_valid_metrics["continuation_reg/batch/valid_msle"] = log_loss.item()
        self.epoch_valid_metrics["continuation_reg/epoch/valid_msle"] += log_loss.item()
        return log_loss

    def validate(self, x, y, y_pred, y_true, model, im=None, task_name=None):
        pass

from pytorch_metric_learning.losses import NTXentLoss, SupConLoss, ContrastiveLoss, TripletMarginLoss

class ContinuationContrastive(Task):
    def __init__(self, param, featuremap, dataset, testset):
        super().__init__("continuation contrastive", param, featuremap, dataset, testset)
        self.epoch_train_metrics = {
            "continuation/epoch_train/contrastive loss": 0,
        }
        self.epoch_valid_metrics = {
            "continuation/epoch_top_k_rmse/top1": 0,
            "continuation/epoch_top_k_rmse/top3": 0,
            "continuation/epoch_top_k_rmse/top5": 0,
            "continuation/epoch_valid/contrastive loss": 0,
        }

        if param["continuation_contrastive_loss"] == "ntxent":
            self.loss_func = NTXentLoss(temperature=param["continuation_temperature"])
        elif param["continuation_contrastive_loss"] == "supcon":
            self.loss_func = SupConLoss(temperature=param["continuation_temperature"])
        elif param["continuation_contrastive_loss"] == "contrastive":
            self.loss_func = ContrastiveLoss(neg_margin=1)
        elif param["continuation_contrastive_loss"] == "triplet":
            self.loss_func = TripletMarginLoss(margin=0.2)

    def get_model_head(self):
        from models.continuation import ContinuationContrastive
        return ContinuationContrastive(self.param)

    def prepare_batch(self, x, y):
        x_masked = x.clone()
        mask_length = self.param["continuation_mask_length"]
        x_masked[:,-mask_length:] = 0
        x_masked[:,-mask_length:,self.featuremap.mask] = 1
        x[:,-mask_length:,self.featuremap.mask] = 1
        return x_masked, x

    def predict(self, x, y, model, task_name=None):
        if task_name:
            x_embeddings_masked = model(x)[task_name]
            x_embeddings = model(y)[task_name] # (batch, dim_embedding_space)
        else:
            x_embeddings_masked = model(x)
            x_embeddings = model(y) # (batch, dim_embedding_space)
        return x_embeddings_masked, x_embeddings


    def loss_train(self, y_pred, y_true):
        batch_size = y_pred.shape[0]
        all_embeddings = torch.cat((y_pred, y_true), dim=0)
        del y_pred, y_true
        arange = torch.arange(0, batch_size, dtype=torch.long, requires_grad=False, device=device)
        labels = torch.cat((arange, arange), dim=0)
        del arange
        loss = self.loss_func(all_embeddings, labels)

        self.batch_train_metrics["continuation/batch_train/contrastive loss"] = loss.mean().item()
        self.epoch_train_metrics["continuation/epoch_train/contrastive loss"] += loss.mean().item()
        return loss

    def loss_valid(self, y_pred, y_true):
        batch_size = y_pred.shape[0]
        all_embeddings = torch.cat((y_pred, y_true), dim=0)
        arange = torch.arange(0, batch_size, dtype=torch.long, requires_grad=False, device=device)
        labels = torch.cat((arange, arange), dim=0)
        loss = self.loss_func(all_embeddings, labels)

        self.batch_valid_metrics["continuation/batch_valid/contrastive loss"] = loss.mean().item()
        self.epoch_valid_metrics["continuation/epoch_valid/contrastive loss"] += loss.mean().item()
        return loss

    def validate(self, x, y, y_pred, y_true, model, im, task_name=None):
        df = self.featuremap.digits_first
        dl = self.featuremap.digits_last+1
        batch_masked_left = x # (batch,sequence,features)
        batch_unmasked_left = y # (batch,sequences,features)
        del x,y

        batch_sequences_left = batch_unmasked_left[:,:,df:dl].unsqueeze(1) # (batch,1,sequence,digits)
        
        if task_name:
            distances, indices = im[task_name].get_nearest_neighbors(batch_masked_left, k=5)
        else:
            distances, indices = im.get_nearest_neighbors(batch_masked_left, k=5)
        topk_sequences = self.dataset[indices.cpu()][0].to(device)

        batch_sequences_right = topk_sequences[:,:,:,df:dl] # (batch,5,sequence,digits)
        topk_square_elem_error = torch.mean(torch.square(batch_sequences_left - batch_sequences_right), dim=3) # (batch,5,sequence)
        topk_mean_square_error = torch.sqrt(torch.mean(topk_square_elem_error, dim=2)) # (batch, 5)

        top1_rmse = topk_mean_square_error[:,0] # (batch,)
        top3_rmse = torch.min(topk_mean_square_error[:,0:3],dim=1)[0] # (batch,)
        top5_rmse = torch.min(topk_mean_square_error[:,0:5],dim=1)[0] # (batch,)

        top1_rmse_mean = top1_rmse.mean().item()
        top3_rmse_mean = top3_rmse.mean().item()
        top5_rmse_mean = top5_rmse.mean().item()

        self.batch_valid_metrics["continuation/batch_top_k_rmse/top1"] = top1_rmse_mean
        self.batch_valid_metrics["continuation/batch_top_k_rmse/top3"] = top3_rmse_mean
        self.batch_valid_metrics["continuation/batch_top_k_rmse/top5"] = top5_rmse_mean
        self.epoch_valid_metrics["continuation/epoch_top_k_rmse/top1"] += top1_rmse_mean
        self.epoch_valid_metrics["continuation/epoch_top_k_rmse/top3"] += top3_rmse_mean
        self.epoch_valid_metrics["continuation/epoch_top_k_rmse/top5"] += top5_rmse_mean
