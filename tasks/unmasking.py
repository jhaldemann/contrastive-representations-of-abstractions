import torch
from tasks.task import Task
from data.settings import device, deftype
import benchmark.loss as L
import benchmark.metrics as M

class UnmaskingRegressive(Task):
    def __init__(self, param, featuremap, dataset, testset):
        super().__init__("unmasking regressive", param, featuremap, dataset, testset)
        self.norm_loss = L.MSENormLoss(self.param, self.featuremap)
        self.sign_loss = L.SIGNLoss(self.param, self.featuremap)
        self.log_loss = L.ModLogLoss(self.param, self.featuremap)
        self.epoch_train_metrics = {
            "unmasking/epoch_train/mse of norm": 0,
            "unmasking/epoch_train/bce of sign": 0,
            "unmasking/epoch_train/mod log": 0,
        }
        self.epoch_valid_metrics = {
            "unmasking/epoch_valid/mse of norm": 0,
            "unmasking/epoch_valid/bce of sign": 0,
            "unmasking/epoch_valid/mod log": 0,
            "unmasking/epoch_valid/mae of logs": 0,
            "unmasking/epoch_valid/re of logs": 0,
            "unmasking/epoch_valid/mae of norms": 0,
            "unmasking/epoch_valid/re of norms": 0,
            "unmasking/epoch_valid/accuracy of signs": 0,
        }

    def get_model_head(self):
        from models.unmasking import UnmaskingRegressive
        return UnmaskingRegressive(self.param)

    def prepare_batch(self, x, y):
        x_masked = x.clone()
        mask_prob = self.param["unmasking_mask_probability"]
        mask = torch.empty(size=(x.shape[0],x.shape[1],1), dtype=deftype, device=device).uniform_(0,1)
        mask = (mask < mask_prob).to(dtype=deftype)
        x_masked *= (1-mask)
        x[:,:,self.featuremap.mask] = mask[:,:,0]
        x_masked[:,:,self.featuremap.mask] = mask[:,:,0]           
        return x_masked, x

    def predict(self, x, y, model, task_name=None):
        if task_name:
            y_pred = model(x)[task_name]
        else:
            y_pred = model(x)
        y_true = y
        return y_pred, y_true

    def loss_train(self, y_pred, y_true):
        mse_of_normalized = self.norm_loss(y_pred, y_true)
        bce_of_sign = self.sign_loss(y_pred, y_true)
        mod_log = self.log_loss(y_pred, y_true)

        self.batch_train_metrics["unmasking/batch_train/mse of norm"] = mse_of_normalized.item()
        self.batch_train_metrics["unmasking/batch_train/bce of sign"] = bce_of_sign.item()
        self.batch_train_metrics["unmasking/batch_train/mod log"] = mod_log.item()

        self.epoch_train_metrics["unmasking/epoch_train/mse of norm"] += mse_of_normalized.item()
        self.epoch_train_metrics["unmasking/epoch_train/bce of sign"] += bce_of_sign.item()
        self.epoch_train_metrics["unmasking/epoch_train/mod log"] += mod_log.item()

        loss = mse_of_normalized + bce_of_sign + mod_log
        return loss

    def loss_valid(self, y_pred, y_true):
        mse_of_normalized = self.norm_loss(y_pred, y_true)
        bce_of_sign = self.sign_loss(y_pred, y_true)
        mod_log = self.log_loss(y_pred, y_true)

        self.batch_valid_metrics["unmasking/batch_valid/mse of norm"] = mse_of_normalized.item()
        self.batch_valid_metrics["unmasking/batch_valid/bce of sign"] = bce_of_sign.item()
        self.batch_valid_metrics["unmasking/batch_valid/mod log"] = mod_log.item()
        self.epoch_valid_metrics["unmasking/epoch_valid/mse of norm"] += mse_of_normalized.item()
        self.epoch_valid_metrics["unmasking/epoch_valid/bce of sign"] += bce_of_sign.item()
        self.epoch_valid_metrics["unmasking/epoch_valid/mod log"] += mod_log.item()
        loss = mse_of_normalized + bce_of_sign + mod_log
        return loss

    def validate(self, x, y, y_pred, y_true, model, im=None, task_name=None):
        mea_of_logs = M.mae_of_logs(y_pred, y_true, self.featuremap)
        re_of_logs = M.re_of_logs(y_pred, y_true, self.featuremap)
        mae_of_normalized = M.mae_of_normalized(y_pred, y_true, self.featuremap)
        re_of_normalized = M.re_of_normalized(y_pred, y_true, self.featuremap)
        accuracy_of_signs = M.accuracy_of_signs(y_pred, y_true, self.featuremap)
        del y_pred,y_true
        self.batch_valid_metrics["unmasking/batch_valid/mae of logs"] = mea_of_logs
        self.batch_valid_metrics["unmasking/batch_valid/re of logs"] = re_of_logs
        self.batch_valid_metrics["unmasking/batch_valid/mae of norms"] = mae_of_normalized
        self.batch_valid_metrics["unmasking/batch_valid/re of norms"] = re_of_normalized
        self.batch_valid_metrics["unmasking/batch_valid/accuracy of signs"] = accuracy_of_signs

        self.epoch_valid_metrics["unmasking/epoch_valid/mae of logs"] += mea_of_logs
        self.epoch_valid_metrics["unmasking/epoch_valid/re of logs"] += re_of_logs
        self.epoch_valid_metrics["unmasking/epoch_valid/mae of norms"] += mae_of_normalized
        self.epoch_valid_metrics["unmasking/epoch_valid/re of norms"] += re_of_normalized
        self.epoch_valid_metrics["unmasking/epoch_valid/accuracy of signs"] += accuracy_of_signs

from pytorch_metric_learning.losses import NTXentLoss, SupConLoss, ContrastiveLoss, TripletMarginLoss

class UnmaskingContrastive(Task):
    def __init__(self, param, featuremap, dataset, testset):
        super().__init__("unmasking contrastive", param, featuremap, dataset, testset)
        self.epoch_train_metrics = {
            "unmasking/epoch_train/contrastive loss": 0,
        }
        self.epoch_valid_metrics = {
            "unmasking/epoch_top_k_rmse/top1": 0,
            "unmasking/epoch_top_k_rmse/top3": 0,
            "unmasking/epoch_top_k_rmse/top5": 0,
            "unmasking/epoch_valid/contrastive loss": 0,
        }

        if param["unmasking_contrastive_loss"] == "ntxent":
            self.loss_func = NTXentLoss(temperature=param["unmasking_temperature"])
        elif param["unmasking_contrastive_loss"] == "supcon":
            self.loss_func = SupConLoss(temperature=param["unmasking_temperature"])
        elif param["unmasking_contrastive_loss"] == "contrastive":
            self.loss_func = ContrastiveLoss(neg_margin=1)
        elif param["unmasking_contrastive_loss"] == "triplet":
            self.loss_func = TripletMarginLoss(margin=0.2)

    def get_model_head(self) -> torch.nn.Module:
        from models.unmasking import UnmaskingContrastive
        return UnmaskingContrastive(self.param)

    def prepare_batch(self, x, y):
        x_masked = x.clone()
        mask_prob = self.param["unmasking_mask_probability"]
        mask = torch.empty(size=(x.shape[0],x.shape[1],1), dtype=deftype, device=device).uniform_(0,1)
        mask = (mask < mask_prob).to(dtype=deftype)
        x_masked *= (1-mask)
        x[:,:,self.featuremap.mask] = mask[:,:,0]
        x_masked[:,:,self.featuremap.mask] = mask[:,:,0]           
        return x_masked, x
        

    def predict(self, x, y, model, task_name=None):
        if task_name:
            x_embeddings_masked = model(x)[task_name]
            x_embeddings = model(y)[task_name] # (batch, dim_embedding_space)
        else:
            x_embeddings_masked = model(x)
            x_embeddings = model(y) # (batch, dim_embedding_space)
        return x_embeddings_masked, x_embeddings

    def loss_train(self, y_pred, y_true):
        batch_size = y_pred.shape[0]
        all_embeddings = torch.cat((y_pred, y_true), dim=0)
        del y_pred, y_true
        arange = torch.arange(0, batch_size, dtype=torch.long, requires_grad=False, device=device)
        labels = torch.cat((arange, arange), dim=0)
        del arange
        loss = self.loss_func(all_embeddings, labels)

        self.batch_train_metrics["unmasking/batch_train/contrastive loss"] = loss.mean().item()
        self.epoch_train_metrics["unmasking/epoch_train/contrastive loss"] += loss.mean().item()
        return loss


    def loss_valid(self, y_pred, y_true):
        batch_size = y_pred.shape[0]
        all_embeddings = torch.cat((y_pred, y_true), dim=0)
        arange = torch.arange(0, batch_size, dtype=torch.long, requires_grad=False, device=device)
        labels = torch.cat((arange, arange), dim=0)
        loss = self.loss_func(all_embeddings, labels)

        self.batch_valid_metrics["unmasking/batch_valid/contrastive loss"] = loss.mean().item()
        self.epoch_valid_metrics["unmasking/epoch_valid/contrastive loss"] += loss.mean().item()
        return loss

    def validate(self, x, y, y_pred, y_true, model, im=None, task_name=None):
        df = self.featuremap.digits_first
        dl = self.featuremap.digits_last+1
        batch_masked_left = x # (batch,sequence,features)
        batch_unmasked_left = y # (batch,sequences,features)
        del x,y

        batch_sequences_left = batch_unmasked_left[:,:,df:dl].unsqueeze(1) # (batch,1,sequence,digits)

        if task_name:
            distances, indices = im[task_name].get_nearest_neighbors(batch_masked_left, k=5)
        else:
            distances, indices = im.get_nearest_neighbors(batch_masked_left, k=5)
        topk_sequences = self.dataset[indices.cpu()][0].to(device)

        batch_sequences_right = topk_sequences[:,:,:,df:dl] # (batch,5,sequence,digits)
        topk_square_elem_error = torch.mean(torch.square(batch_sequences_left - batch_sequences_right), dim=3) # (batch,5,sequence)
        topk_mean_square_error = torch.sqrt(torch.mean(topk_square_elem_error, dim=2)) # (batch, 5)

        top1_rmse = topk_mean_square_error[:,0] # (batch,)
        top3_rmse = torch.min(topk_mean_square_error[:,0:3],dim=1)[0] # (batch,)
        top5_rmse = torch.min(topk_mean_square_error[:,0:5],dim=1)[0] # (batch,)

        top1_rmse_mean = top1_rmse.mean().item()
        top3_rmse_mean = top3_rmse.mean().item()
        top5_rmse_mean = top5_rmse.mean().item()

        self.batch_valid_metrics["unmasking/batch_top_k_rmse/top1"] = top1_rmse_mean
        self.batch_valid_metrics["unmasking/batch_top_k_rmse/top3"] = top3_rmse_mean
        self.batch_valid_metrics["unmasking/batch_top_k_rmse/top5"] = top5_rmse_mean
        self.epoch_valid_metrics["unmasking/epoch_top_k_rmse/top1"] += top1_rmse_mean
        self.epoch_valid_metrics["unmasking/epoch_top_k_rmse/top3"] += top3_rmse_mean
        self.epoch_valid_metrics["unmasking/epoch_top_k_rmse/top5"] += top5_rmse_mean