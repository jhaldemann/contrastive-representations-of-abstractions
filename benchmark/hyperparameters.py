hyperparameters_sweep = {
    "name": "Transformer",
    "method": "grid",
    "metric": {
        "name": "score",
        "goal": "minimize",
    },
    "parameters": {
        "lr": {
            "values": [1e-4, 1e-5, 1e-6]
        },
        # "lr_decay": {
        #     "values": [0.99, 0.95, 0.9, 0.8]
        # },
    }
}

def get_hyperparameters(parameters, hyperparameters):
    p = {}
    result = hyperparameters.copy()
    result["name"] = result["name"] + "_" + str(parameters["jobid"])
    # reshape to wandb-format
    for key, value in parameters.items():
        p[key] = {
            "value": value
        }
    result["parameters"] = p # append parameters to result
    # override hyperparameters
    for key, value in hyperparameters["parameters"].items():
        result["parameters"][key] = value
    return result


def hyperparameters(parameters):
    return get_hyperparameters(parameters, hyperparameters_sweep)