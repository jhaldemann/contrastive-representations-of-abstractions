import argparse
initial_parameters = {
    # processing
    "validation_split": 0.1,
    # general parameters
    "dim_sequence": 50,
    "dim_classes": 7,
    "seed": 12345,
}
debug_parameters = {
    # processing
    "debug_dataset_fraction": 0.001,
    # dataset init
    "debug_training_fraction": 0.1,
    "debug_validation_fraction": 0.1,
    # training
    "debug_epochs": 4,
    "debug_train_batch_size": 16,
    "debug_valid_batch_size": 16,
}
default_parameters = {
    # normal training
    "training_fraction": 1.0,
    "validation_fraction": 0.01,
    # sweeping
    "sweep_fraction": 0.01, # both validation & training fraction
    # batch size for normal training & sweeping
    "train_batch_size": 120,
    "valid_batch_size": 120,
    # general
    "directory": "D:",
    # "directory": "/itet-stor/jhaldemann/net_scratch/",
    "wandb_entity": "jhaldemann",
    "train_model": True,
    "eval_model": True,
    "only_last": False, # only evaluate last epoch
    "save_model": True,
    "optimizer": "adamw",
    # transformer
    "transformer_normalising": True,
    "transformer_position_encoding": True,
    # task unmasking
    "unmasking_mask_probability": 0.25,
    "unmasking_regressive_dim_layers": [2000],
    "unmasking_contrastive_dim_layers": [2000],
    "unmasking_contrastive_dim_embedding_space": 100,
    "unmasking_contrastive_loss": "supcon",
    "unmasking_temperature": 0.2,
    # task continuation
    "continuation_mask_length": 5,
    "continuation_regressive_dim_layers": [2000],
    "continuation_contrastive_dim_layers": [2000],
    "continuation_dim_embedding_space": 100, ## optimized would be 100
    "continuation_contrastive_loss": "supcon",
    "continuation_temperature": 0.1,
    # task classification
    "classification_confidence_threshold": 0.55,
    "classification_regressive_dim_layers": [2000],
    "classification_contrastive_dim_layers": [2000],
    "classification_contrastive_dim_embedding_space": 100,
    "classification_contrastive_loss": "supcon",
    "classification_temperature": 0.4,
    # task complexity
    "complexity_regressive_dim_layers": [2000],
    "complexity_contrastive_dim_layers": [2000],
    "complexity_contrastive_dim_embedding_space": 100,
    "complexity_contrastive_loss": "supcon",
    "complexity_contrastive_positive": 2, # number of positive complexity pairs
    "complexity_contrastive_pos_distance_margin": 1,
    "complexity_temperature": 0.2,
    # task next
    "next_dim_layers": [2000],
    # pre-training
    "contrastive_pretrain": True,
    "task_switch": 6, # for attaskauto
    # logging
    "verbosity": 1,
    "wandbosity": 0,
    # use debugging or sweeping
    "debug": True,
    "sweep": False,
}

def parameters():
    parser = setup_parser()
    return {**initial_parameters, **debug_parameters, **default_parameters, **vars(parser.parse_args())}

def setup_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-e",
        "--epochs",
        type=int,
        default=12,
        help="number of epochs."
    )
    parser.add_argument(
        "--lr",
        type=float,
        default=1e-4,
        help="learning rate (default=1e-4)"
    )
    parser.add_argument(
        "--lr_decay",
        type=float,
        default=0.95,
        help="exponential learning rate decay (default=0.99)"
    )
    parser.add_argument(
        "--l2_regularization",
        type=float,
        default=1e-4,
        help="weight decay regularization (default=1e-4)"
    )
    parser.add_argument(
        "--leave_one_out",
        type=str,
        choices=["unmasking", "continuation", "classification","complexity","next"],
        help="pre-training-task to leave out for leave-one-out-analysis."
    )
    parser.add_argument(
        "--leave_one_out_feature",
        type=str,
        choices=["position", "sign","normalized","log","digits","mask"],
        help="pre-training-features to leave out for leave-one-out-analysis."
    )
    parser.add_argument(
        "--transformer_dim_heads",
        type=int,
        default=4,
    )
    parser.add_argument(
        "--transformer_dim_layers",
        type=int,
        default=4,
    )
    parser.add_argument(
        "--transformer_dim_feedforward",
        type=int,
        default=2000,
    )
    parser.add_argument(
        "--transformer_dropout",
        type=float,
        default=0.1,
    )
    parser.add_argument(
        "-r",
        "--regime",
        type=str,
        default="inepoch",
        choices=["inbatch","inepoch","atepoch","attask","attaskauto"],
        help="choose the training-regime"
    )
    parser.add_argument(
        "-nh",
        "--no_head",
		action="store_true",
		help="dont use projection head during pre-training"
    )
    parser.add_argument(
        "--load_tuning",
		action="store_true",
		help="load a tuning model"
    )
    parser.add_argument(
        "-t",
        "--task",
        type=str,
        default="unmasking",
        choices=["unmasking", "classification","complexity","next","continuation"],
        help="choose which task to use."
    )
    parser.add_argument(
		"-c",
		"--contrastive",
		action="store_true",
		help="use contrastive version of the task"
    )
    parser.add_argument(
		"-j",
		"--jobid",
		type=str,
		default="local",
		help="the job id (and the id of the wandb run)"
	)
    parser.add_argument(
        "-m",
        "--model",
        type=str,
        default="new",
        help="model id or new model (default=new)"
    )
    parser.add_argument(
        "-f",
        "--model_freeze",
    	action="store_false",
		help="freeze parameters of a prepended model (default=true)"
    )
    return parser