#!/bin/bash

#SBATCH --mail-type=NONE
#SBATCH --output=/home/jhaldemann/repos/sp/log/%j.out
#SBATCH --error=/home/jhaldemann/repos/sp/log/%j.err
#SBATCH --gres=gpu:1
#SBATCH --mem=70G


# Exit on errors
set -o errexit

TMPDIR=$(mktemp -d)
if [[ ! -d ${TMPDIR} ]]; then
    echo 'Failed to create temp directory' >&2
    exit 1
fi
trap "exit 1" HUP INT TERM
trap 'rm -rf "${TMPDIR}"' EXIT
export TMPDIR

# Change the current directory to the location where you want to store temporary files, exit if changing didn't succeed.
# Adapt this to your personal preference
cd "${TMPDIR}" || exit 1

if [ -z "${SLURM_JOB_ID}" ];
then
	SLURM_JOB_ID="local"
fi
echo "Running on node: $(hostname)"
echo "In directory:    $(pwd)"
echo "Starting on:     $(date)"
echo "SLURM_JOB_ID:    ${SLURM_JOB_ID}"
# Binary or script to execute
python3 -m pretraining ${@} -j ${SLURM_JOB_ID}

echo "Finished at:     $(date)"

# End the script with exit code 0
exit 0